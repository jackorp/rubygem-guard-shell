# Generated from guard-shell-0.7.1.gem by gem2rpm -*- rpm-spec -*-
%global gem_name guard-shell

Name: rubygem-%{gem_name}
Version: 0.7.1
Release: 1%{?dist}
Summary: Guard gem for running shell commands
License: MIT
URL: http://github.com/hawx/guard-shell
Source0: https://rubygems.org/gems/%{gem_name}-%{version}.gem
# git clone https://github.com/guard/guard-shell.git && cd guard-shell
# git checkout v0.7.1
# tar -czvf rubygem-shell-0.7.1-spec.tar.gz spec/
Source1: %{name}-%{version}-spec.tar.gz
BuildRequires: ruby(release)
BuildRequires: rubygems-devel
BuildRequires: ruby
BuildRequires: rubygem(rspec)
BuildRequires: rubygem(guard-compat)
BuildRequires: rubygem(guard)
BuildArch: noarch

%description
Guard::Shell automatically runs shell commands when watched files are
modified.


%package doc
Summary: Documentation for %{name}
Requires: %{name} = %{version}-%{release}
BuildArch: noarch

%description doc
Documentation for %{name}.

%prep
gem unpack %{SOURCE0}

%setup -q -D -T -n  %{gem_name}-%{version} -b 1

gem spec %{SOURCE0} -l --ruby > %{gem_name}.gemspec

%build
# Create the gem as gem install only works on a gem file
gem build %{gem_name}.gemspec

# %%gem_install compiles any C extensions and installs the gem into ./%%gem_dir
# by default, so that we can move it into the buildroot in %%install
%gem_install

%install
mkdir -p %{buildroot}%{gem_dir}
cp -a .%{gem_dir}/* \
        %{buildroot}%{gem_dir}/


# Run the test suite.
%check
pushd .%{gem_instdir}
ln -s %{_builddir}/spec spec
rspec spec
popd

%files
%dir %{gem_instdir}
%license %{gem_instdir}/LICENSE
%{gem_libdir}
%exclude %{gem_cache}
%{gem_spec}

%files doc
%doc %{gem_docdir}
%doc %{gem_instdir}/Readme.md

%changelog
* Tue Jan 23 2018 Jaroslav Prokop <jar.prokop@volny.cz> - 0.7.1-1
- Initial package
